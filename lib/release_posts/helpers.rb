# frozen_string_literal: true

module ReleasePosts
  module Helpers
    Abort = Class.new(StandardError)
    Done = Class.new(StandardError)

    MAX_FILENAME_LENGTH = 99 # GNU tar has a 99 character limit

    def capture_stdout(cmd)
      IO.popen(cmd, &:read)
    end

    def fail_with(message)
      raise Abort, "\e[31merror\e[0m #{message}"
    end

    def git_add(src)
      capture_stdout(['git', 'add', src])
    end

    def git_config(key, value)
      capture_stdout(['git', 'config', key, value])
    end

    def git_fetch
      capture_stdout(%w[git fetch])
    end

    def git_merge(branch)
      capture_stdout(['git', 'merge', '--no-ff', branch])
    end

    def git_commit(message)
      capture_stdout(['git', 'commit', '--message', message])
    end

    def git_push(path, branch)
      capture_stdout(['git', 'push', path, "HEAD:#{branch}"])
    end

    def git_mv(src, dst)
      capture_stdout(['git', 'mv', src, dst])
      $stdout.puts "\e[34mmoved\e[0m #{src} to #{dst}"
    end

    def replace(old, new, file)
      expr = "%s##{old}##{new}#|x"
      $stdout.puts "\e[34mchanged\e[0m #{old} path to #{new} in #{file}"
      capture_stdout(['ex', '-sc', expr, file])
    end

    def git_change_branch(branch)
      capture_stdout(['git', 'checkout', branch])
    end

    def git_branch_list(pattern)
      capture_stdout(['git', 'branch', '-r', '--list', pattern])
    end
  end
end
