---
layout: handbook-page-toc
title: "Field Functional Competencies"
---

# Field Functional Competencies  
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# What are Field Functional Competencies? 

GitLab field functional competencies provide a common language and consistent expectations for “what good looks like” for the GitLab field organization (inclusive of Sales and Customer Success roles). They are intended to complement [GitLab’s values](/handbook/values/) and are comprised of specific behaviors that GitLab team members in a field role need to demonstrate consistently to be successful. These critical field skills and behaviors are not meant to be role or job-specific (as each role or job may have additional unique requirements), and how each competency is expressed or demonstrated may vary by role (e.g. by technical depth, engagement in different stages of the customer journey, team-specific expectations, etc.). 

More specifically, functional competencies help to:
*  Guide how GitLab field team members are selected, onboarded, developed, promoted, and rewarded (above & beyond hitting their quota)
*  Build organizational capabilities when adopted and practiced across the field organization
*  Reinforce behavior that is consistent with GitLab’s values and culture
*  Enable GitLab field team members to build productive relationships and generate positive results

There are three primary competencies for the field: Customer Focus, Solution Focus, and Operational Excellence. Please refer to the tables below to learn more. 

## Customer Focus

Field team members should be focused on the customer and how we can deliver a world class experience with every interaction. 


| SKILL                              | BEHAVIOR DESCRIPTION                                                                                                                                                                                                                                                                                                                                                           |
|------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Understanding the Customer's Needs | Keeps the customer and their goals in mind when engaging with them. Seek to understand their Positive Business Outcomes and how they will measure success. Using this information to anticipate the customer’s business needs to provide support and drive adoption of their GitLab technologies.                                                                             |
| Presenting                         | Shares knowledge with customers in a way that creates credibility, demonstrates integrity, and conveys GitLab values. Adjusts presentation based on audience level and interests. Is able to effectively tell a story to capture their audience, and be able to tell engaging stories as part of presentation. Crisp, concise communication, delivered in person or remotely. |
| Facilitation (Customer Presence)   | Project confidence, credibility, and conviction in body language, voice, and words during meetings to show interest, gain respect, and inspire trust. Can effectively manage a room and facilitate beneficial discussion                                                                                                                                                       |
| Customer Business Acumen           | Understands the dynamics, trends, and challenges of the customer's business and their industry. Then advocates internally for the needs of the customer.                                                                                                                                                                                                                       |
| Active Listening                   | Leverages active listening to create mutual understanding between GitLab Team and customer regarding their short-term and long-term business challenges. Uses MEDDPICC to actively understand content and emotional messaging to show interest, connect, learn, and build trust with the customer.                                                                             |



| RATING LEVEL | DESCRIPTION OF THE RATING LEVEL                                                                                                                                                                                                                             |
|--------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Learning     | Practices: Holds business oriented conversations with customer and understands where to go to get questions answered. Requires direction, training, and/ or feedback in order to effectively grow customer relationships.                                   |
| Growing      | Applies: Comfortably engages customers in insightful business conversations. Requires limited guidance and feedback in order to sustain and grow customer relationships.                                                                                    |
| Thriving     | Integrates: Seamlessly moves conversations towards strategic goals. Regularly provides direction, guidance, training, and/or feedback to others in developing customer relationships.                                                                       |
| Expert       | Innovates: Sought after by customers and viewed by the industry as a valued “voice.” Works with Field Enablement Team and Field Ops to develop, improve, and/or introduce new approaches that positively represent GitLab to the customer and the industry. |



## Solution Focus

Field team members should be focused on how GitLab helps customers solve their business problems and how they can help drive their success.

| SKILL                           | BEHAVIOR DESCRIPTION                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Consultative Approach           | The ability to diagnose customer needs and determine the solution for new and existing customers to help them achieve their strategic business goals. Uses Command of the Message and MEDDPICC to create dialogue to uncover, explore, shape, and deliver on the needs of customers. Uses Command of the Message and MEDDPICC to demonstrate value and connect GitLab to customer's business pain.                                                                                |
| Set Vision & Drive Adoption     | Creates effective strategies to create a clear path for success, and documents them within Command Plan or Success Plan.                                                                                                                                                                                                                                                                                                                                       |
| DevOps and Technical Leadership | Comprehensive understanding of how products and services integrate to form solutions in the context of the customer's ecosystem. Drive DevOps and the solution that includes the people, process, technology. Be able to speak to Agile DevOps methodology. Use agile DevOps in every task and build automation with this method.,Bring ideas and collaborate with other team members and customers about how they can be more Agile using DevOps methods. |
| Building Trust (Trusted Advisor) | Uses consultative skills to ensure GitLab's products meet specific customer's needs as a partner to the customer rather than another vendor. Show up as a credible and reliable champion for your customers, by being able to connect with the customer on a personal and professional level, to drive success.                                                                                                                                                |



| RATING LEVEL | DESCRIPTION OF RATING LEVEL                                                                                                                                                                                                                                                                                |
|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Learning     | Practices: Understands all stages of GitLab and is able to articulate how they form basic solutions for the customer. Requires direction, training, and/or feedback to effectively have these conversations with customers.                                                                                |
| Growing      | Applies: Understands the products within the GitLab Portfolio, in the context of the customer’s needs/goals. Drives conversations within GitLab on the customer’s behalf. Requires limited guidance and feedback in order to drive adoption.                                                               |
| Thriving     | Integrates: Combines product and industry knowledge to advise and propose solutions that align closely with a customer's strategic It vision. Regularly provides direction, guidance, training, and/ or feedback or other is driving adoption.                                                              |
| Expert       | Innovates: Leverages customer relationships to consistently uncover and pursue strategic opportunities with customers where solutions increase revenue, cut costs, and provide business edge. Develops, improves, and/ or introduces new approaches and tools that position GitLab as a solution provider. |


## Operational Excellence
 
To drive efficiency, field team members should adhere to the tools and processes established by the Field Operations team and their leadership.

| SKILL                         | BEHAVIOR DESCRIPTION                                                                                                                                                                                                                                                                                                                                                                                 |
|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Process Discipline            | The ability to evaluate, implement, and manage the GitLab Customer Success Rhythm of Business to understand and contribute to GitLab’s long term strategic initiatives. This includes managing time, priorities and daily workload to effectively engage customers. Effectively uses tools to support the development and execution of the account plan. Actions should be repeatable and scalable. |
| Account Planning & Management | Creates effective strategies for expanding within an account including a clear plan customer journey selling and adoption and key plays (i.e., CI, CD, Security, Jenkins and Microsoft takeout, Premium/Ultimate).                                                                                                                                                                                   |
| Resource Orchestration        | Engages partners and internal resources and leverages tools at the right time and in the right manner to move the customer forward while prioritizing time and resources.                                                                                                                                                                                                                                |
| Growth Mindset                | Explore and foster the mindset necessary to continuously develop and take risks. Curiosity and perseverance are parts of this skill.                                                                                                                                                                                                                                                                 |


| RATING   | DESCRIPTION OF RATING LEVEL                                                                                                                                                                                                                   |
|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Learning | Practices: Practices all aspects of the process for your role. Requires direction, training, and/or feedback to consistently use process and tools.                                                                               |
| Growing  | Applies: Applies all aspects of the process for your role. Requires limited guidance and feedback to use process and tools consistently and effectively.                                                                          |
| Thriving | Integrates: Fully integrates all aspects of the process for your role into your daily work. Regularly provides direction, guidance, training, and/or feedback to others in learning, using, and optimizing the process and tools. |
| Expert   | Innovates: Works with the Sales & Customer Enablement Team to develop, improve, and/or introduce new approaches that positively impact the speed, efficiency, and/ or effectiveness of the business processes within Customer Success.        |

