---
layout: handbook-page-toc
title: "Recruiting Process - Recruiting Operations & Insights Tasks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Recruiting Process Framework - Recruiting Operations & Insights Tasks

### System Access

To gain access to a system listed below, please follow the respective link to the appropriate page on how to go about submitting an

* **ContactOut**
    * Sourcing Team only
* **DocuSign**
    * C.E.S. and Sales Operations Team 
* **Greenhouse**
* **LinkedIn Recruiter**

### System Processes

* **DocuSign**
    * TBA
* **Greenhouse**
    * Candidate Profile Merge Requests
    * Offer and Requisition Approvals
    * Referral Submissions

### Reporting

* **Monthly Metrics Reports**
* **Weekly, Monthly, and Quarterly Reports**
* **Ad Hoc Reports**

### Common Issues

* **LinkedIn Recruiter**
* **Greenhouse Offer and Requisition Approvals**
* **System Integrations**
* **Re-Opening Requisitions**
