# Monorepo

## FAQ

* QUESTION: I'm getting a Middleman build error like `Error: Could not locate ...`
  * ANSWER: Add the file which could not be located to `/data/monorepo.yml`, under the `shared_files` list, under the correct `site`. Please be consistent: 1) alphabetize, and 2) add a comment indicating where the file is used from.

## Background

See the following epic and its sub-epics and issues for context on the monorepo:

* https://gitlab.com/groups/gitlab-com/-/epics/282
* Summary: "To ensure stability, accountability and efficiency we need to separate and isolate the various sub-sites in the www-gitlab-com repo."

## Why

* There are many reasons large organizations, including Google, Microsoft, and Facebook, use monorepos (TODO: add references).
* A monorepo approach is useful when you want to separate and isolate different areas ownership and functionality, but they still have dependencies or relationships among each other.  For example, shared libraries, assets, documentation, etc.
* One of the main reasons is because of versioning: coordinating related across different areas.
  * In the "separate but related areas" situation, separate repositories introduce significant additional process and technical complexity compared to a single repo.
  * This is because in a single repository, you have the git SHA as a natural "version" to coordinate related changes across different areas, and this coordination can be handled via single Merge Requests.
  * With multiple repositories, in order to coordinate changes, you must have multiple MRs to coordinate related changes, and you must introduce some sort of versioning scheme to indicate which versions of the multiple repos "go together".
  * You must also introduce more complex deployment strategies to ensure that the correct versions and dependencies are deployed together, as well as more complex deprecation schemes to know when you can deprecate old versions which no longer have dependencies from other repos.
* These concepts behind these motivations are also reflected elsewhere in GitLab's technical strategy and documentation:
  * [Advantages of a single application](https://about.gitlab.com/handbook/product/single-application/)
  * [Database Strategy](https://about.gitlab.com/handbook/engineering/development/enablement/database/doc/strategy.html) 
    * [The trouble with microservices](https://about.gitlab.com/handbook/engineering/development/enablement/database/doc/strategy.html#the-trouble-with-microservices)

## Overview

* There is a single git repo which contains multiple "sites", which live under `sites/<sitename>`
* Files (layouts, media, data, common config, etc) which are shared among the sites, or not specific to any site, live at the top-level of the repo, not under any `site/...` directory. (NOTE: These may eventually be all moved under a top-level `shared` directory.)
* However, there is still a single website/domain built and published from all the individual sites. (NOTE: this may eventually change, if different sites decide to implement independent deployments and/or domains)
* The `data/monorepo.yml` file is used as a **"single source of truth"** for all the monorepo sites and most of their configuration:
  * It contains a top-level list of the sites, which must match the directories under `sites/`
  * Under each site, there are two entries: `paths` and `shared_files`.
  * `paths` describes what URL paths are served by the site.
  * `shared_files` describes what top-level files are referenced by the site.
* There is a top-level Middleman build `config.rb` file, as well as dedicated `sites/<sitename>/config.rb` builds for each site.  More details on this below.

## "No Circular Dependencies" Rule

The primary rule is that there must be no "circular dependencies" in the monorepo at build time.  This means:

* Files in individual sites **MAY** depend on (directly reference) shared files in the top-level at build time.
* Files in individual sites **MUST NOT** depend on (directly reference) files in any other site at build time.  The only exception to this is "runtime" references, such as a URL, image link, or iframe pointing to a file or page in the built/published website, which was built and published by a different site.
* Top-level files **MUST NOT** depend on files in any other site at build time (with the same "runtime" reference exception as above).

## Middleman Support for Monorepo

Middleman does not provide official support for building a single site which is composed out of sub-sites in a monorepo structure.  However, it does provide some low-level support for configuring a monorepo structure to work.

We have encapsulated and abstracted most of this configuration, so that knowledge of the low-level middleman details is not necessary for day-to-day development.  This is driven via the `data/monorepo.yml` referenced above.

There is a custom Middleman extension, `extensions/monorepo.rb`, which encapsulates the necessary config, driven by `data/monorepo.yml`.  It does the following:

* Uses each sites' `paths` entry to implement a `manipulate_resource_list` hook, and only publish the specified paths for that site.
* Uses each sites' `shared_files` entry to add a `files.watch` configuration, which includes only those shared files as dependencies.
* Add some other common middleman configuration which is required for the monorepo structure to work.

Each site has a dedicated Middleman build config at `sites/<sitename>/config.rb`, which calls the `monorepo` extension, as well as other necessary configuration such as proxy resources for the site.  However, in Middleman `development` mode, these are not used - everything is built by the top-level `config.rb`.  More details below.

## Middleman Build Mode vs. Development Mode

When the site is being built for deployment via CI/CD, different parts of the site are built by different Middleman builds. Currently, this includes everything in the top-level `source` directory (built via the `partial_build` extension), plus all the individual sites' Middleman builds.  Note that eventually, once all pages are moved to monorepo sites, there will be no need for `partial_build`.

However, in `development` mode, when you run `middleman` (not `middleman build`) to start the dev server from the root of a local repo, this serves **EVERYTHING**, including all sites.  It does **NOT** use the individual sites' `config.rb` builds.

**BUT**, the top level `config.rb` **does** include the proxy resource definitions from the sub-sites, which are in shared config files named `sites/<sitename>/config_proxy_resources.rb`.  Each site's `sites/sitename/config.rb` also includes these, so the proxy resource definitions are [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) and not duplicated. 
 

## Migrating a new site to the monorepo

* Look at the issues for the previous monorepo migrations. There are detailed task lists there:
  * [The initial spike and research into the viability of a monorepo approach under Middleman](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/49086)
  * [Handbook Part 1](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6689) - Note this also has many associated MRs that were done as part of the initial cleanup and research.
  * [Handbook Part 2](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7912)
  * [Blog/Releases](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52779) (currently still in progress and evolving, was put on hold to finish Handbook Part 2)
* Use `scripts/monorepo-migration-helper.rb`.  But note that it is not perfect! It currently will miss transitive dependencies, and may add some unnecessary dependencies which are referenced in markdown code blocks.
