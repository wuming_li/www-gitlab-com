---
layout: markdown_page
title: "Category Direction - Secrets Management"
description: Our vision is to embed Vault within GitLab and migrate to using it for our own secrets management, within the GitLab Core as well as for your CI Runners. 
canonical_path: "/direction/release/secrets_management/"
---

- TOC
{:toc}

## Secrets Management

HashiCorp's [Vault](https://www.vaultproject.io/use-cases/secrets-management/) is paving the way for OSS Secrets Management and many of our customers are leveraging the solution today.  Vault lets you easily rotate secrets and can manage intermediate, temporary tokens used between different services. This ensures there are no long-term tokens lying
around or commonly used. Vault minimizes GitLab's attack surface and protects 
against any unknown zero-day vulnerabilities in our Rails app today or those that allow a bad actor to access the Gitlab server by ensuring that GitLab does not hold any long term secrets. 

See the [introduction to Vault](https://www.youtube.com/watch?v=pURiZLl6o3w) and our discussion on [Vault<>GitLab Integration](https://www.youtube.com/watch?v=9kD3geEmSJ8) with our CEO @systes.  

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASecrets%20Management)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3ASecrets%20Management) - [Research Insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3ASecrets%20Management)

## Secrets Management & Vault Use Cases 

There are three main secrets management use cases that Vault will solve for. These are as follows: 

- Keeping GitLab's own secrets safe as seen in ([gitlab&1319](https://gitlab.com/groups/gitlab-org/-/epics/1319))
- Storing secrets for CI variables and jobs which can be followed in ([gitlab&816](https://gitlab.com/groups/gitlab-org/-/epics/816))
- Managing secrets in general, in own Vault or one provided by GitLab, as illustrated by a collection of features including the Bundling of Vault with Gitlab ([omnibus-gitlab#4317](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4317)), a GitLab.com provided instance ([gitlab#28584](https://gitlab.com/gitlab-org/gitlab/issues/28584)) and a built-in manager for Vault secrets ([gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)). 

## Target audience and experience

Operations, compliance, security, and audit teams will derive immense value from being able to manage secrets within GitLab. Vault will expand GitLab's security by offering an extra layer for tokens, keys, and other confidential data. This combination of tools will further establish GitLab's presence as an enterprise-grade, corporate solution for Release Management. 

## What's next & why

Now that we can effectively authenticate Vault with GitLab from ([gitlab#9983](https://gitlab.com/gitlab-org/gitlab/issues/9983)), and users can install Vault into a Kubernetes clusters via ([gitlab#9982](https://gitlab.com/gitlab-org/gitlab/issues/9982)), we are devoting more time to handling CI Variables from Vault. Users can now authenticate using a JSON Web Token via ([gitlab#207125](https://gitlab.com/gitlab-org/gitlab/issues/207125)), to then fetch and read secrets from Vault via the implementation of ([gitlab#28321](https://gitlab.com/gitlab-org/gitlab/issues/28321)). 

Check out the 13.0 release of the GitLab <> HashiCorp Vault Integration: 
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Zsdt3Irgjus" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Maturity Plan

This category is currently at the "Minimal" maturity level, and our next maturity target is "Viable": (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables to achieve this are:

- [Allow a user to install Vault in Kubernetes Cluster](https://gitlab.com/gitlab-org/gitlab/issues/9982) (Complete)
- [Generate JWT for authentication and provide it to CI jobs](https://gitlab.com/gitlab-org/gitlab/issues/207125) (Complete)
- [New `secrets` syntax](https://gitlab.com/gitlab-org/gitlab/-/issues/218746) (13.2)
- [Extend Runner to pass secrets](https://gitlab.com/gitlab-org/gitlab/-/issues/212252) (13.2)
- [GitLab reads from Vault for CI Variables](https://gitlab.com/gitlab-org/gitlab/issues/28321) (13.2)
- [Define Vault credentials in UI](https://gitlab.com/gitlab-org/gitlab/-/issues/218677) 
- [Split secrets from CI/CD Variables](https://gitlab.com/gitlab-org/gitlab/-/issues/217355)

## Competitive landscape

There are other secrets management stores in the market. There is a nice overview
of [Vault vs. KMS](https://www.hashicorp.com/resources/how-vault-compare-cloud-kms/) which contains a
lot of information about why we believe Vault is a better solution for secrets
management. We could consider in the future also supporting different solutions
such as KMS.

Additionally, [Vault Enterprise](https://www.vaultproject.io/docs/enterprise/) offers
additional sets of capabilities that will _not_ be part of the open source version
of Vault bundled with GitLab. This includes 
[replication across datacenters](https://www.vaultproject.io/docs/enterprise/replication/index.html),
[hardware security modules (HSMs)](https://www.vaultproject.io/docs/enterprise/hsm/index.html),
[seals](https://www.vaultproject.io/docs/enterprise/sealwrap/index.html),
[namespaces](https://www.vaultproject.io/docs/enterprise/namespaces/index.html),
[servicing read-only requests on HA nodes](https://www.vaultproject.io/docs/enterprise/performance-standby/index.html)
(though, the open source version does support [high-availability](https://www.vaultproject.io/docs/concepts/ha.html)),
[enterprise control groups](https://www.vaultproject.io/docs/enterprise/control-groups/index.html),
[multi-factor auth](https://www.vaultproject.io/docs/enterprise/mfa/index.html),
and [sentinel](https://www.vaultproject.io/docs/enterprise/sentinel/index.html).

For customers who want to use GitLab with the enterprise version of Vault, we need
to ensure that this is easy to switch to/use as well.

## Top Customer Success/Sales issue(s)

The top focus for most accounts is to be able to easily integrate with an existing Vault instance. We will render the most value from this after we deliver ([gitlab#28321](https://gitlab.com/gitlab-org/gitlab/-/issues/28321)), which will support the fetching and reading of secrets from Vault for CI jobs. Fit and finish features like managing secrets in the GitLab UI as described in ([gitlab#218677](https://gitlab.com/gitlab-org/gitlab/-/issues/218677)) will reduce the barriers of entry between using Vault and GitLab together.

## Top user issue(s)

Our most popular issue is managing Vault secrets inside Gitlab ([gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)). We will likely deliver on ([gitlab#218677](https://gitlab.com/gitlab-org/gitlab/-/issues/218677)) to define Vault configuration in GitLab for CI/CD jobs and measure consumption before investing in a greater UI experience. 

We have considered bundling Vault in Omnibus via ([omnibus-gitlab#4317](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4317)), due to it's popularity. Upon futher investigation, many of our users have an existing Vault and there is little tolerance for reconfiguring to use a GitLab provided Vault. Delivering a best in class integration and managed application will deliver on this need. 

Our infrastructure and SecOps teams are proceeding to invest in [moving GitLab's own secrets into Vault](https://gitlab.com/groups/gitlab-org/-/epics/1319).

Expanding the prescriptive use cases for Vault in GitLab prior to investing in development effort is currently being considered in ([gitlab-org&2365](https://gitlab.com/groups/gitlab-org/-/epics/2365)).  

## Top internal customer issue(s)

Internally, once the Vault integration is available we can begin moving some of
the secrets tracked internally in GitLab to the included Vault.

- [Examine which secrets we can move to Vault from gitlab.rb](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4297)
- [Discontinue using attr_encrypted](https://gitlab.com/gitlab-org/gitlab/issues/26243)
- [Allow the `db_key_base` secret to be rotated](https://gitlab.com/gitlab-org/gitlab/issues/25332)

The MVC for migrating our internal secrets is being tracked in the epic to [move GitLab's own secrets into Vault](https://gitlab.com/groups/gitlab-org/-/epics/1319). Supporting GitLab internal secrets, does require the Vault integration being mandatory as part of the install first.

## Top Vision Item(s)

Secrets management is a must-have for enterprise-grade release governance. Adding a proper interface to Vault ([gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)) embedded in GitLab, would make it easier to interact with the Vault instance. The interface can be leveraged for all secrets, which would also be a competitive feature set for the Operations-centered and security minded buyers within the regulated space. 

Features to deliver on the complaince and regulatory needs of Secrets Mangement include expansion of Audit events via ([gitlab#8070](https://gitlab.com/gitlab-org/gitlab/-/issues/8070)) and the redefinition of a secret as a seperate entity in ([gitlab#217355](https://gitlab.com/gitlab-org/gitlab/-/issues/217355)). Secrets are treated differently than CI/CD variables in practice although GitLab does not distinguish the experience for our users. Investing in this separation and tracking will help our position in [the Zero Trust Wave](https://www.forrester.com/playbook/The+Zero+Trust+Security+Playbook+For+2020/-/E-PLA300) and support our user's in their compliance journey.
